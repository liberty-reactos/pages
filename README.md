# STBC

Web repository of libre software for [![ReactOS](https://raw.githubusercontent.com/reactos/press-media/master/Art/Website/Current/roswebsitebutton.png)](https://reactos.org/)! <!-- FIXME: the web button should be hosted in this repository, while retaining a note about the original source. -->

*Standing up to be counted.*

## License

Published via the [WTFPL version 2](LICENSE).

## Similar initiatives

- [The web version of packmgr](https://web.archive.org/web/20080605232849/http://frik85.fr.funpic.de/packmgr/), an **[ancient package manager for ReactOS](https://web.archive.org/web/20080904001719/http://www.reactos.org/wiki/index.php/ReactOS_Package_Manager)** - *note that it's not a strictly libre project.*
- [RAPPS](https://reactos.org/wiki/RAPPS), the **official package manager of ReactOS**. Native GUI approach, similar to POSIX software, e.g. [Synaptic](https://wiki.debian.org/Synaptic), [OctoPkg](https://tintaescura.com/projects/octopkg/), etc. [Critized for suggesting proprietary software](https://www.gnu.org/distros/common-distros.html#ReactOS).